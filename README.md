# Common Voice aurkezpenak

Informazio gehiagorako: https://www.iametza.eus/software-librea-eta-common-voice-egitasmoa-gazteen-artean-zabaltzen/

## Lizentzia

[GNU Affero General Public License v3.0](https://www.gnu.org/licenses/licenses.html#AGPL)
